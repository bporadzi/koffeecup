import { Directive, Input, ElementRef, AfterViewInit, Output, EventEmitter, Renderer2 } from '@angular/core';

const viewportOffset = '200px';

@Directive({
  selector: '[appLazyLoadImage]'
})
export class LazyLoadImageDirective implements AfterViewInit {

  @Input('appLazyLoadImage')
  imageSrc: string;

  @Output()
  imageLoaded: EventEmitter<void> = new EventEmitter();

  private intersectionObserver: IntersectionObserver;

  constructor(
    private element: ElementRef,
    private renderer: Renderer2,
  ) { }

  ngAfterViewInit() {
    const observerOptions: IntersectionObserverInit = {
      rootMargin: viewportOffset,
      threshold: 0.5,
    };

    this.intersectionObserver = new IntersectionObserver((entries) => this.onViewportEnter(entries), observerOptions);

    this.intersectionObserver.observe(this.element.nativeElement);
  }

  onViewportEnter(entries: Array<IntersectionObserverEntry>): void {
    entries.forEach((entry: IntersectionObserverEntry) => this.loadImageIfInViewport(entry));
  }

  private loadImageIfInViewport(entry: IntersectionObserverEntry): void {
    if (!entry.isIntersecting || entry.target !== this.element.nativeElement) { return; }

    this.renderer.removeAttribute(this.element.nativeElement, 'data-src');

    this.loadImage();

    this.intersectionObserver.unobserve(this.element.nativeElement);
    this.intersectionObserver.disconnect();
    this.imageLoaded.emit();
  }

  private loadImage(): void {
    if (this.element.nativeElement instanceof HTMLImageElement) {
      this.renderer.setAttribute(this.element.nativeElement, 'src', this.imageSrc);
    } else {
      this.renderer.setStyle(this.element.nativeElement, 'background-image', `url(${this.imageSrc})`);
    }
  }

}
