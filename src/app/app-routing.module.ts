import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImagesGridComponent } from './views/images-grid/images-grid.component';


const routes: Routes = [
  { path: '', redirectTo: 'images', pathMatch: 'full' },
  { path: 'images', component: ImagesGridComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
