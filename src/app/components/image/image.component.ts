import { Component, Input } from '@angular/core';
import { Image } from 'src/app/interfaces/image.interface';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent {

  @Input()
  image: Image;

  isLoading: boolean = true;

}
