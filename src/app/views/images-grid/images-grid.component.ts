import { Component, OnInit } from '@angular/core';
import { ImagesApiService } from './images-api.service';
import { Observable } from 'rxjs';
import { Image } from '../../interfaces/image.interface';

@Component({
  selector: 'app-images-grid',
  templateUrl: './images-grid.component.html',
  styleUrls: ['./images-grid.component.scss'],
  providers: [ImagesApiService],
})
export class ImagesGridComponent implements OnInit {

  images$: Observable<Image[]>;

  constructor(
    private imagesApi: ImagesApiService,
  ) { }

  ngOnInit(): void {
    this.images$ = this.imagesApi.fetchImagesList();
  }

}
