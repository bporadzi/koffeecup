import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Image } from '../../interfaces/image.interface';

@Injectable()
export class ImagesApiService {

  imagesListUrl: string = 'https://jsonplaceholder.typicode.com/albums/1/photos?_limit=10';

  constructor(
    private http: HttpClient,
  ) { }

  fetchImagesList(): Observable<Image[]> {
    return this.http.get<Image[]>(this.imagesListUrl);
  }

}
